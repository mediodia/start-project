document.addEventListener(
	"DOMContentLoaded", () => {
			const menu = new MmenuLight(
					document.querySelector( "#my-menu" ), // Название класса обёртки меню
					"(max-width: 1400px)" // Минимальная ширина экрана на которой меню будет скрыто
			);

			const navigator = menu.navigation({
				"title": "", // Заголовок который появляется вверху когда открываешь меню
			});
			const drawer = menu.offcanvas();

			document.querySelector( "a[href='#my-menu']" ) // Кнопа открытия меню
					.addEventListener( "click", ( evnt ) => {
							evnt.preventDefault();
							drawer.open();
					});
	}
);

$(".btn_close").click(function(e) {
  // e.preventDefault();
  $(".mm-ocd").removeClass('mm-ocd--open');
})

