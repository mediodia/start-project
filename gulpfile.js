var gulp = require('gulp'), // Подключаем Gulp
	sass = require('gulp-sass'), // Подключаем Sass
	browserSync = require('browser-sync'), //автоперезагрузка при изменении кода
	autoprefixer = require('gulp-autoprefixer'), // Автопрефиксы
	cleanCSS = require('gulp-clean-css'), // Минифицирует и оптемизирует код
	gcmq = require('gulp-group-css-media-queries'), // Объединяет медиа запросы
	beautify = require("gulp-beautify"), // Приводит в порядок код
	concat = require('gulp-concat'), // Склеивает js в один файл
	uglify = require('gulp-uglify'), // Минифицирует js
	include = require('gulp-include'), // Подключение шаблонов (типа tpl)
	rsync = require('gulp-rsync'), // Деплой на хостинг
	newer = require('gulp-newer'), // Оптимизация изображейни (newer и responsive работают в паре)
	responsive = require('gulp-responsive'), // Оптимизация изображейни (newer и responsive работают в паре)
	svgmin = require('gulp-svgmin'), // Оптимизация svg
	removeHtmlComments = require('gulp-remove-html-comments'); //Удаляем комментарии из html



// Включение browserSync
gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: 'dist/'
		},
		// proxy: "opencart.loc", // если сайт на openserver = закомментировать server:{}, раскомменитовать proxy 
		notify: false
	})
});


// SCSS -> CSS + оптимизация кода + автоперезагрузка браузера
gulp.task('style', function () { // Создаем таск "style" (назвать можно поразному)
	return gulp.src('src/assets/scss/*.scss') // Берем источник
		.pipe(sass({
			outputStyle: 'expanded'
		}).on('error', sass.logError)) // Преобразуем SCSS в CSS
		// .pipe(autoprefixer()) // Автопрефикс
		.pipe(gcmq()) // Объединяет медиа запросы и ставит их в конец
		.pipe(cleanCSS({
			// format: 'beautify',
			level: {
				2: {
					all: false,
					mergeAdjacentRules: true, // Объединяет дублирующиеся селекторы
					removeEmpty: true, // Удаляет пустые селекторы
				}
			}
		}))
		.pipe(beautify.css({
			indent_size: 2
		}))
		.pipe(gulp.dest('dist/assets/css')) // Выгружаем результата в указанную папку
		.pipe(browserSync.stream()) // Автоперезагрузка браузера
});


// JS
gulp.task('js', function() {
	return gulp.src([
		'src/assets/js/main.js',
		// 'src/assets/libs/slide-menu/dist/slide-menu.js',
		'src/assets/libs/mmenu-light/dist/mmenu-light.js',
		// 'src/assets/libs/mmenu-js-master/dist/mmenu.js',
		
		])
	.pipe(concat('main.js'))
	// .pipe(uglify()) // Mifify js (opt.)
	.pipe(gulp.dest('dist/assets/js'))
	// .pipe(browserSync.reload({ stream: true }))
		.pipe(browserSync.stream()) // Автоперезагрузка браузера
});


// Сборка шаблонов .html
gulp.task('code', function () {
	return gulp.src(['!src/_copypast.html', 'src/*.html']) // От куда брать html для сборкию Путь указанный с "!" исключается из компиляции.
		.pipe(include())
		.on('error', console.log)
		.pipe(removeHtmlComments())
		.pipe(gulp.dest('dist/')) // Куда вставляем скомпилированный html
		.pipe(browserSync.stream()) // Автоперезагрузка браузера
});


// Деплой на сервер (команда для деплоя - "gulp deploy")
gulp.task('deploy', function () {
	return gulp.src('folder/**') // адрес того, что деплоим (тот же адрес и у "root:", но без "**"). Нельзя указывать просто "/"
		.pipe(rsync({
			root: 'folder/',
			hostname: 'SSHlogin@SSHhost', // логин от ssh@хост от ssh
			destination: '/homepages/26/d782790008/htdocs/backup/', // адрес на сервере куда делать деплой
			// include: ['/bb.sh'], // Includes files to deploy
			// exclude: ['node_modules'], // Excludes files from deploy
			recursive: true,
			archive: true,
			silent: false,
			compress: true
		}))
});


// Оптимизация изображений
gulp.task('img', function () {
	return gulp.src('dist/assets/img/**/*')
		.pipe(newer('src/assets/img/'))
		.pipe(responsive({
			'**/*': {
				width: '100%',
				quality: 85
				// progressive: true
			}
		}))
		.pipe(gulp.dest('public/assets/img/'));
});

// Оптимизация svg
gulp.task('svg', function () {
	return gulp.src(['!src/assets/img/folder/*', 'src/assets/img/**/*'])
		.pipe(svgmin())
		.pipe(gulp.dest('dist/assets/img/'));
});

// Какой таск за какими папками следит
gulp.task('watch', function () {
	gulp.watch('src/assets/scss/*.scss', gulp.parallel('style'));
	gulp.watch('src/assets/js/*.js', gulp.parallel('js'));
	// gulp.watch('src/*.html', gulp.parallel('code')); 
	gulp.watch(['src/*.html', 'src/assets/html/**/*'], gulp.parallel('code'));
});

// Какие таски включаются при команде "gulp"
gulp.task('default', gulp.parallel('style', 'browser-sync', 'code', 'js','watch'));